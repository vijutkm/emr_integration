﻿using System.Collections.Generic;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using HL7.Dotnetcore;
using System.Net;
using System;
using WriteText;
using HL7Lib;

namespace EMR.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {        
        private string HL7_ORM;
        Status obj = new Status();
        public ObjectResult HL7Test()
        {
            var path = @"E:\ASHNA SERVICE PROJECT\ashna\EMRIntegration\EMRIntegration/";
            HL7_ORM = System.IO.File.ReadAllText(path + "Sample-ORM.txt");
            HL7.Dotnetcore.Message message = new HL7.Dotnetcore.Message(HL7_ORM);
            bool isParsed = false;
            try
            {
                isParsed = message.ParseMessage();
                List<Segment> segList = message.Segments();
                String SendingFacility =Convert.ToString( message.getValue("MSH.4"));
                String SendingFacility1 = message.DefaultSegment("MSH").Fields(4).Value;
                WriteXml.write_xml(SendingFacility);
            }
            catch (Exception ex)
            {
                throw;
            }
            return Created("", obj);
        }

    }
}
