﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for log
/// </summary>
namespace WriteText
{
    public static class WriteXml
    {


        public static void write_xml(string xmlstring)
        {
            string path = "c:\\log\\";
            string xmlname = "log.txt";
            //string subPath = Convert.ToString(patient_id);
            //string pathname = string.Concat(path, "\\");
            string basePath = path;
            bool exists = System.IO.Directory.Exists(basePath);
            if (!exists)
                System.IO.Directory.CreateDirectory(basePath);

            string Path1 = string.Concat(xmlname, ".xml");
            string filePath = basePath + "\\" + Path1;
            System.IO.File.WriteAllText(filePath, xmlstring);
        }
    }
}